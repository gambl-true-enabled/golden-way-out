package com.golden.way.out.net

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory
import java.security.cert.CertificateException
import java.util.concurrent.TimeUnit
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager

object NetworkModule {

    private var mRetrofit: Retrofit? = null

    fun init() {
        try {
            mRetrofit = Retrofit.Builder()
                .baseUrl(getBaseUrl())
                .addCallAdapterFactory(CoroutineCallAdapterFactory.invoke())
                .client(okHttp())
                .addConverterFactory(convertFactory(objectMapper()))
                .build()
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }

    private fun getBaseUrl(): String = "http://ec2-3-138-179-26.us-east-2.compute.amazonaws.com/"

    fun getRetrofit(): Retrofit? = mRetrofit

    private fun objectMapper(): ObjectMapper {
        val mapper = ObjectMapper()

        mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false)
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)

        return mapper
    }

    private fun convertFactory(mapper: ObjectMapper): retrofit2.Converter.Factory {
        return JacksonConverterFactory.create(mapper)
    }

    private fun okHttp(): OkHttpClient {
        try {
            val trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager {
                @Throws(CertificateException::class)
                override fun checkClientTrusted(
                    chain: Array<java.security.cert.X509Certificate>,
                    authType: String
                ) {
                }

                @Throws(CertificateException::class)
                override fun checkServerTrusted(
                    chain: Array<java.security.cert.X509Certificate>,
                    authType: String
                ) {
                }

                override fun getAcceptedIssuers(): Array<java.security.cert.X509Certificate> {
                    return arrayOf()
                }
            })
            val sslContext = SSLContext.getInstance("SSL")
            sslContext.init(null, trustAllCerts, java.security.SecureRandom())
            val sslSocketFactory = sslContext.socketFactory

            val builder = OkHttpClient.Builder().apply {
                connectTimeout(2, TimeUnit.MINUTES)
                sslSocketFactory(sslSocketFactory, trustAllCerts[0] as X509TrustManager)
                hostnameVerifier { _, _ -> true }
            }
            return builder.build()
        } catch (e: Exception) {
            throw RuntimeException(e)
        }
    }
}