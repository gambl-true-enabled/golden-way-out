package com.golden.way.out.net

import com.fasterxml.jackson.annotation.*
import java.util.*

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder(
    "customer_user_id",
    "custom_data",
    "media_source",
    "campaign",
    "event_value",
    "event_name"
)
class RawData {

    @get:JsonProperty("customer_user_id")
    @set:JsonProperty("customer_user_id")
    @JsonProperty("customer_user_id")
    var customerUserId: String? = null

    @get:JsonProperty("custom_data")
    @set:JsonProperty("custom_data")
    @JsonProperty("custom_data")
    var customData: String? = null

    @get:JsonProperty("media_source")
    @set:JsonProperty("media_source")
    @JsonProperty("media_source")
    var mediaSource: String? = null

    @get:JsonProperty("campaign")
    @set:JsonProperty("campaign")
    @JsonProperty("campaign")
    var campaign: String? = null

    @get:JsonProperty("event_value")
    @set:JsonProperty("event_value")
    @JsonProperty("event_value")
    var eventValue: String? = null

    @get:JsonProperty("event_name")
    @set:JsonProperty("event_name")
    @JsonProperty("event_name")
    var eventName: String? = null
}