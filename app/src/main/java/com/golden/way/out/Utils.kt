package com.golden.way.out

import android.content.Context
import android.webkit.JavascriptInterface

private const val WAY_TABLE = "com.WAY.table.123"
private const val WAY_ARGS = "com.WAY.value.456"

class JavaScript(
    private val callback: Callback
) {

    interface Callback {
        fun needAuth()
        fun authorized()
    }

    @JavascriptInterface
    fun onAuthorized() {
        callback.authorized()
    }

    @JavascriptInterface
    fun onNeedAuth() {
        callback.needAuth()
    }
}

fun Context.saveLink(deepArgs: String) {
    val sharedPreferences = getSharedPreferences(WAY_TABLE, Context.MODE_PRIVATE)
    sharedPreferences.edit().putString(WAY_ARGS, deepArgs).apply()
}

fun Context.getArgs(): String? {
    val sharedPreferences = getSharedPreferences(WAY_TABLE, Context.MODE_PRIVATE)
    return sharedPreferences.getString(WAY_ARGS, null)
}
